General preprocessing. See subdirectories for specific analyses.

General:
========

List of 50 individuals:

```bash
rm 50individuals.txt
for i in {0..49}; do
  echo "tsk_$i" >> 50individuals.txt
done
```

For LDhat:
==========

```bash
wget https://github.com/auton1/LDhat/raw/master/lk_files/lk_n50_t0.001.gz
wget https://github.com/auton1/LDhat/raw/master/lk_files/lk_n100_t0.001.gz
gunzip lk_n50_t0.001.gz
gunzip lk_n100_t0.001.gz
lkgen -lk lk_n50_t0.001 -nseq 10
mv new_lk.txt lk_n10_t0.001 
```

For Pyrho:
==========

Generate a lookup table (constant pop size):

```bash
pyrho make_table --samplesize 10 \
                 --mu 1.25e-8 \
                 --popsizes 100000,100000 \
                 --epochtimes 100 \
                 --numthreads 20 \
                 --logfile . \
                 --outfile ConstantPopSize_10.hdf

pyrho make_table --samplesize 100 \
                 --approx --moran_pop_size 128 \
                 --mu 1.25e-8 \
                 --popsizes 100000,100000 \
                 --epochtimes 100 \
                 --numthreads 20 \
                 --logfile . \
                 --outfile ConstantPopSize_100.hdf
```

Note: we could also generate the table from the MSMC output. Here we assume we know the real demography.

Optimize hyper parameters:

```bash
pyrho hyperparam --samplesize 10 \
                 --tablefile ConstantPopSize_10.hdf \
                 --mu 1.25e-8 \
                 --ploidy 1 \
                 --popsizes 100000,100000 \
                 --epochtimes 100 \
                 --numthreads 20 \
                 --logfile . \
                 --blockpenalty 25,50,100 \
                 --windowsize 30,60,90 \
                 --num_sims 3 \
                 --outfile ConstantPopSize_10_hyperparam_results.txt

pyrho hyperparam --samplesize 100 \
                 --tablefile ConstantPopSize_100.hdf \
                 --mu 1.25e-8 \
                 --ploidy 1 \
                 --popsizes 100000,100000 \
                 --epochtimes 100 \
                 --numthreads 20 \
                 --logfile . \
                 --blockpenalty 25,50,100 \
                 --windowsize 30,60,90 \
                 --num_sims 3 \
                 --outfile ConstantPopSize_100_hyperparam_results.txt
```

With a decreasing population size: we assume the demography is known and we approximate the true one by a skyline model.
The pop size decreases from 1e5 1000 generations ago to 1000 individuals today, with an exponential decrease.
This gives (considering 20 time intervals):

```r
g = - log(1e5 / 1e3) / (1000 - 0)
t <- round(seq(0, 1e3, length.out = 20))
p <- round(1e5 * exp(g*(1000-t)))
plot(p~t)
paste(t, collapse = ",")
paste(p, collapse = ",")
```

```bash
pyrho make_table --samplesize 10 \
                 --mu 1.25e-8 \
                 --popsizes 1000,1276,1622,2070,2642,3357,4285,5445,6950,8872,11272,14388,18365,23335,29785,37844,48306,61660,78343,100000 \
                 --epochtimes 53,105,158,211,263,316,368,421,474,526,579,632,684,737,789,842,895,947,1000 \
                 --numthreads 20 \
                 --logfile . \
                 --outfile DecreasingPopSize_10.hdf

pyrho make_table --samplesize 100 \
                 --approx --moran_pop_size 128 \
                 --mu 1.25e-8 \
                 --popsizes 1000,1276,1622,2070,2642,3357,4285,5445,6950,8872,11272,14388,18365,23335,29785,37844,48306,61660,78343,100000 \
                 --epochtimes 53,105,158,211,263,316,368,421,474,526,579,632,684,737,789,842,895,947,1000 \
                 --numthreads 20 \
                 --logfile . \
                 --outfile DecreasingPopSize_100.hdf
```

Optimize hyper parameters:

```bash
pyrho hyperparam --samplesize 10 \
                 --tablefile DecreasingPopSize_10.hdf \
                 --mu 1.25e-8 \
                 --ploidy 1 \
                 --popsizes 1000,1276,1622,2070,2642,3357,4285,5445,6950,8872,11272,14388,18365,23335,29785,37844,48306,61660,78343,100000 \
                 --epochtimes 53,105,158,211,263,316,368,421,474,526,579,632,684,737,789,842,895,947,1000 \
                 --numthreads 20 \
                 --logfile . \
                 --blockpenalty 25,50,100 \
                 --windowsize 30,60,90 \
                 --num_sims 3 \
                 --outfile DecreasingPopSize_10_hyperparam_results.txt

pyrho hyperparam --samplesize 100 \
                 --tablefile DecreasingPopSize_100.hdf \
                 --mu 1.25e-8 \
                 --ploidy 1 \
                 --popsizes 1000,1276,1622,2070,2642,3357,4285,5445,6950,8872,11272,14388,18365,23335,29785,37844,48306,61660,78343,100000 \
                 --epochtimes 53,105,158,211,263,316,368,421,474,526,579,632,684,737,789,842,895,947,1000 \
                 --numthreads 20 \
                 --logfile . \
                 --blockpenalty 25,50,100 \
                 --windowsize 30,60,90 \
                 --num_sims 3 \
                 --outfile DecreasingPopSize_100_hyperparam_results.txt
```


With an increasing population size: we assume the demography is known and we approximate the true one by a skyline model.
The pop size increases from 5e4 1000 generations ago to 2e5 individuals today, with an exponential increase.
This gives (considering 20 time intervals):

```r
g = - log(5e4 / 2e5) / (1000 - 0)
t <- round(seq(0, 1e3, length.out = 20))
p <- round(5e4 * exp(g*(1000-t)))
plot(p~t)
paste(t, collapse = ",")
paste(p, collapse = ",")
```

```bash
pyrho make_table --samplesize 10 \
                 --mu 1.25e-8 \
                 --popsizes 200000,185832,172907,160659,149278,138896,129056,120080,111574,103670,96460,89627,83278,77486,71997,66989,62244,57834,53812,50000 \
                 --epochtimes 53,105,158,211,263,316,368,421,474,526,579,632,684,737,789,842,895,947,1000 \
                 --numthreads 20 \
                 --logfile . \
                 --outfile IncreasingPopSize_10.hdf

pyrho make_table --samplesize 100 \
                 --approx --moran_pop_size 128 \
                 --mu 1.25e-8 \
                 --popsizes 200000,185832,172907,160659,149278,138896,129056,120080,111574,103670,96460,89627,83278,77486,71997,66989,62244,57834,53812,50000 \
                 --epochtimes 53,105,158,211,263,316,368,421,474,526,579,632,684,737,789,842,895,947,1000 \
                 --numthreads 20 \
                 --logfile . \
                 --outfile IncreasingPopSize_100.hdf
```

Optimize hyper parameters:

```bash
pyrho hyperparam --samplesize 10 \
                 --tablefile IncreasingPopSize_10.hdf \
                 --mu 1.25e-8 \
                 --ploidy 1 \
                 --popsizes 200000,185832,172907,160659,149278,138896,129056,120080,111574,103670,96460,89627,83278,77486,71997,66989,62244,57834,53812,50000 \
                 --epochtimes 53,105,158,211,263,316,368,421,474,526,579,632,684,737,789,842,895,947,1000 \
                 --numthreads 20 \
                 --logfile . \
                 --blockpenalty 25,50,100 \
                 --windowsize 30,60,90 \
                 --num_sims 3 \
                 --outfile IncreasingPopSize_10_hyperparam_results.txt

pyrho hyperparam --samplesize 100 \
                 --tablefile IncreasingPopSize_100.hdf \
                 --mu 1.25e-8 \
                 --ploidy 1 \
                 --popsizes 200000,185832,172907,160659,149278,138896,129056,120080,111574,103670,96460,89627,83278,77486,71997,66989,62244,57834,53812,50000 \
                 --epochtimes 53,105,158,211,263,316,368,421,474,526,579,632,684,737,789,842,895,947,1000 \
                 --numthreads 20 \
                 --logfile . \
                 --blockpenalty 25,50,100 \
                 --windowsize 30,60,90 \
                 --num_sims 3 \
                 --outfile IncreasingPopSize_100_hyperparam_results.txt
```

Another scenario: the pop size increases from 1e3 1000 generations ago to 1e5 individuals today, with an exponential increase.
This gives (considering 20 time intervals):

```r
g = - log(1e3 / 1e5) / (1000 - 0)
t <- round(seq(0, 1e3, length.out = 20))
p <- round(1e3 * exp(g*(1000-t)))
plot(p~t)
paste(t, collapse = ",")
paste(p, collapse = ",")
```

```bash
pyrho make_table --samplesize 10 \
                 --mu 1.25e-8 \
                 --popsizes 100000,78343,61660,48306,37844,29785,23335,18365,14388,11272,8872,6950,5445,4285,3357,2642,2070,1622,1276,1000 \
                 --epochtimes 53,105,158,211,263,316,368,421,474,526,579,632,684,737,789,842,895,947,1000 \
                 --numthreads 20 \
                 --logfile . \
                 --outfile IncreasingPopSize2_10.hdf

pyrho make_table --samplesize 100 \
                 --approx --moran_pop_size 128 \
                 --mu 1.25e-8 \
                 --popsizes 100000,78343,61660,48306,37844,29785,23335,18365,14388,11272,8872,6950,5445,4285,3357,2642,2070,1622,1276,1000 \
                 --epochtimes 53,105,158,211,263,316,368,421,474,526,579,632,684,737,789,842,895,947,1000 \
                 --numthreads 20 \
                 --logfile . \
                 --outfile IncreasingPopSize2_100.hdf
```

Optimize hyper parameters:

```bash
pyrho hyperparam --samplesize 10 \
                 --tablefile IncreasingPopSize2_10.hdf \
                 --mu 1.25e-8 \
                 --ploidy 1 \
                 --popsizes 100000,78343,61660,48306,37844,29785,23335,18365,14388,11272,8872,6950,5445,4285,3357,2642,2070,1622,1276,1000 \
                 --epochtimes 53,105,158,211,263,316,368,421,474,526,579,632,684,737,789,842,895,947,1000 \
                 --numthreads 20 \
                 --logfile . \
                 --blockpenalty 25,50,100 \
                 --windowsize 30,60,90 \
                 --num_sims 3 \
                 --outfile IncreasingPopSize2_10_hyperparam_results.txt

pyrho hyperparam --samplesize 100 \
                 --tablefile IncreasingPopSize2_100.hdf \
                 --mu 1.25e-8 \
                 --ploidy 1 \
                 --popsizes 100000,78343,61660,48306,37844,29785,23335,18365,14388,11272,8872,6950,5445,4285,3357,2642,2070,1622,1276,1000 \
                 --epochtimes 53,105,158,211,263,316,368,421,474,526,579,632,684,737,789,842,895,947,1000 \
                 --numthreads 20 \
                 --logfile . \
                 --blockpenalty 25,50,100 \
                 --windowsize 30,60,90 \
                 --num_sims 3 \
                 --outfile IncreasingPopSize2_100_hyperparam_results.txt
```

Yet another growth scenario: the pop size increases from 1e4 100,000 generations ago to 2e5 individuals today, with an exponential increase.
This gives (considering 20 time intervals):

```r
g = - log(1e4 / 2e5) / (1e5 - 0)
t <- round(seq(0, 1e5, length.out = 20))
p <- round(1e4 * exp(g*(1e5-t)))
plot(p~t)
paste(t, collapse = ",")
paste(p, collapse = ",")
```

```bash
pyrho make_table --samplesize 10 \
                 --mu 1.25e-8 \
                 --popsizes 200000,170827,145910,124626,106445,90918,77656,66329,56654,48390,41331,35302,30153,25754,21998,18789,16048,13707,11708,10000 \
                 --epochtimes 5263,10526,15789,21053,26316,31579,36842,42105,47368,52632,57895,63158,68421,73684,78947,84211,89474,94737,100000 \
                 --numthreads 20 \
                 --logfile . \
                 --outfile AncientIncreasingPopSize_10.hdf

pyrho make_table --samplesize 100 \
                 --approx --moran_pop_size 128 \
                 --mu 1.25e-8 \
                 --popsizes 200000,170827,145910,124626,106445,90918,77656,66329,56654,48390,41331,35302,30153,25754,21998,18789,16048,13707,11708,10000 \
                 --epochtimes 5263,10526,15789,21053,26316,31579,36842,42105,47368,52632,57895,63158,68421,73684,78947,84211,89474,94737,100000 \
                 --numthreads 20 \
                 --logfile . \
                 --outfile AncientIncreasingPopSize_100.hdf
```

Optimize hyper parameters:

```bash
pyrho hyperparam --samplesize 10 \
                 --tablefile AncientIncreasingPopSize_10.hdf \
                 --mu 1.25e-8 \
                 --ploidy 1 \
                 --popsizes 200000,170827,145910,124626,106445,90918,77656,66329,56654,48390,41331,35302,30153,25754,21998,18789,16048,13707,11708,10000 \
                 --epochtimes 5263,10526,15789,21053,26316,31579,36842,42105,47368,52632,57895,63158,68421,73684,78947,84211,89474,94737,100000 \
                 --numthreads 20 \
                 --logfile . \
                 --blockpenalty 25,50,100 \
                 --windowsize 30,60,90 \
                 --num_sims 3 \
                 --outfile AncientIncreasingPopSize_10_hyperparam_results.txt

pyrho hyperparam --samplesize 100 \
                 --tablefile AncientIncreasingPopSize_100.hdf \
                 --mu 1.25e-8 \
                 --ploidy 1 \
                 --popsizes 200000,170827,145910,124626,106445,90918,77656,66329,56654,48390,41331,35302,30153,25754,21998,18789,16048,13707,11708,10000 \
                 --epochtimes 5263,10526,15789,21053,26316,31579,36842,42105,47368,52632,57895,63158,68421,73684,78947,84211,89474,94737,100000 \
                 --numthreads 20 \
                 --logfile . \
                 --blockpenalty 25,50,100 \
                 --windowsize 30,60,90 \
                 --num_sims 3 \
                 --outfile AncientIncreasingPopSize_100_hyperparam_results.txt
```

With lower population size:

Generate a lookup table (constant pop size):

```bash
pyrho make_table --samplesize 10 \
                 --mu 1.25e-8 \
                 --popsizes 10000,10000 \
                 --epochtimes 100 \
                 --numthreads 20 \
                 --logfile . \
                 --outfile ConstantPopSize10000_10.hdf

pyrho make_table --samplesize 100 \
                 --approx --moran_pop_size 128 \
                 --mu 1.25e-8 \
                 --popsizes 10000,10000 \
                 --epochtimes 100 \
                 --numthreads 20 \
                 --logfile . \
                 --outfile ConstantPopSize10000_100.hdf
```

Optimize hyper parameters:

```bash
pyrho hyperparam --samplesize 10 \
                 --tablefile ConstantPopSize10000_10.hdf \
                 --mu 1.25e-8 \
                 --ploidy 1 \
                 --popsizes 10000,10000 \
                 --epochtimes 100 \
                 --numthreads 20 \
                 --logfile . \
                 --blockpenalty 25,50,100 \
                 --windowsize 30,60,90 \
                 --num_sims 3 \
                 --outfile ConstantPopSize10000_10_hyperparam_results.txt

pyrho hyperparam --samplesize 100 \
                 --tablefile ConstantPopSize10000_100.hdf \
                 --mu 1.25e-8 \
                 --ploidy 1 \
                 --popsizes 10000,10000 \
                 --epochtimes 100 \
                 --numthreads 20 \
                 --logfile . \
                 --blockpenalty 25,50,100 \
                 --windowsize 30,60,90 \
                 --num_sims 3 \
                 --outfile ConstantPopSize10000_100_hyperparam_results.txt
```


For HeRho:
==========

Get the scripts:
https://github.com/samebdon/heRho/blob/main/heRho/heRho_tally_pairwise_counts_vcf.py
https://github.com/samebdon/heRho/blob/main/heRho/heRho_stand_alone.py

```bash
wget https://raw.githubusercontent.com/samebdon/heRho/main/heRho/heRho_tally_pairwise_counts_vcf.py
wget https://raw.githubusercontent.com/samebdon/heRho/main/heRho/heRho_stand_alone.py
```

Use the fix described here:
https://github.com/samebdon/heRho/issues/4
to solve isuue with numerical chr.

Cleaning:
=========

To save space, we compress some result files:

LDhat output files. Check disk usage:

```bash
du -ch */*/LDhat/*/*/res*.txt 
```
=> ~8 Gb

Now compress:

```bash
gzip -f */*/LDhat/*/*/res*.txt
du -ch */*/LDhat/*/*/res* 
```
=> 1.4 Gb

```bash
du -ch */*/Pyrho/*/*/*.rmap
``
=> 5 Gb

Now compress:

```bash
gzip -f */*/Pyrho/*/*/*.rmap
du -ch */*/Pyrho/*/*/*.rmap.gz
```
=> 850 Mb

