<!-- Created on 05/04/21 by jdutheil -->

Data simulation
===============

```bash
python3.9 simulate.py --rec=0.15e-8 --output=sim_rec1
python3.9 simulate.py --rec=0.75e-8 --output=sim_rec2
python3.9 simulate.py --rec=1.5e-8 --output=sim_rec3
python3.9 simulate.py --rec=7.5e-8 --output=sim_rec4
python3.9 simulate.py --rec=1.5e-8 --gc-prop=0.1 --gc-track=300  --output=sim_rec3_gc10_300bp
python3.9 simulate.py --rec=1.5e-8 --gc-prop=0.5 --gc-track=300  --output=sim_rec3_gc50_300bp
python3.9 simulate.py --rec=1.5e-8 --gc-prop=0.9 --gc-track=300  --output=sim_rec3_gc90_300bp
python3.9 simulate.py --rec=1.5e-8 --gc-prop=1.0 --gc-track=300  --output=sim_rec3_gc100_300bp
```

Outputs:
```
DemographyDebugger
╠══════════════════════════════════╗
║ Epoch[0]: [0, 1e+03) generations ║
╠══════════════════════════════════╝
╟    Populations (total=1 active=1)
║    ┌─────────────────────────────────────────┐
║    │     │    start│        end│growth_rate  │
║    ├─────────────────────────────────────────┤
║    │  Pop│   1000.0│   100000.0│-0.00461     │
║    └─────────────────────────────────────────┘
╟    Events @ generation 1e+03
║    ┌──────────────────────────────────────────────────────────────────────────┐
║    │  time│type        │parameters       │effect                              │
║    ├──────────────────────────────────────────────────────────────────────────┤
║    │  1000│Population  │population=Pop,  │growth_rate → 0 for population Pop  │
║    │      │parameter   │growth_rate=0    │                                    │
║    │      │change      │                 │                                    │
║    └──────────────────────────────────────────────────────────────────────────┘
╠════════════════════════════════════╗
║ Epoch[1]: [1e+03, inf) generations ║
╠════════════════════════════════════╝
╟    Populations (total=1 active=1)
║    ┌───────────────────────────────────────────┐
║    │     │      start│        end│growth_rate  │
║    ├───────────────────────────────────────────┤
║    │  Pop│   100000.0│   100000.0│ 0           │
║    └───────────────────────────────────────────┘
```

Compute genetic diversity (using all individuals):

```bash
compute_diversity() {
  echo "Computing diversity for ${1}..."
  for rep in {0..9}; do
    mkdir -p Diversity/$1/rep$rep/
    cd Diversity/$1/rep$rep/

    echo "$1, replicate $rep"

    vcftools --gzvcf ../../../Output/$1/rep$rep/${1}_rep${rep}.vcf.gz \
             --window-pi 10000000 \
             --out ${1}_rep${rep}_50indv

    cd ../../..
  done
}

compute_diversity sim_rec1
compute_diversity sim_rec2
compute_diversity sim_rec3
compute_diversity sim_rec4
compute_diversity sim_rec3_gc10_300bp
compute_diversity sim_rec3_gc50_300bp
compute_diversity sim_rec3_gc90_300bp
compute_diversity sim_rec3_gc100_300bp
```

Estimate rho using MSMC
=======================

For MSMC, extract 5 individuals:

```bash
extract_individuals() {
  echo "Extracting individuals for ${1}..."
  for rep in {0..9}; do
    for i in tsk_0 tsk_1 tsk_2 tsk_3 tsk_4; do
      echo "$rec $rep $i"
      cd Output/$1/rep$rep/
      vcftools --gzvcf ${1}_rep${rep}.vcf.gz --indv $i --out ${1}_rep${rep}_${i} --recode
      bgzip -f ${1}_rep${rep}_${i}.recode.vcf
      cd ../../..
    done
  done
}

extract_individuals sim_rec1
extract_individuals sim_rec2
extract_individuals sim_rec3
extract_individuals sim_rec4
extract_individuals sim_rec3_gc10_300bp
extract_individuals sim_rec3_gc50_300bp
extract_individuals sim_rec3_gc90_300bp
extract_individuals sim_rec3_gc100_300bp
```

We then generate the MSMC input files, combining all individuals:

```bash
format_msmc() {
  echo "Formating MSMC files for ${1}..."
  for rep in {0..9}; do
    mkdir -p MSMC/$1/rep$rep/
    python3 ../../generate_multihetsep.py \
        Output/$1/rep$rep/${1}_rep${rep}_tsk_0.recode.vcf.gz \
        > MSMC/$1/rep$rep/${1}_rep${rep}_1indv.msmc
    python3 ../../generate_multihetsep.py \
        Output/$1/rep$rep/${1}_rep${rep}_tsk_0.recode.vcf.gz \
        Output/$1/rep$rep/${1}_rep${rep}_tsk_1.recode.vcf.gz \
        Output/$1/rep$rep/${1}_rep${rep}_tsk_2.recode.vcf.gz \
        Output/$1/rep$rep/${1}_rep${rep}_tsk_3.recode.vcf.gz \
        Output/$1/rep$rep/${1}_rep${rep}_tsk_4.recode.vcf.gz \
        > MSMC/$1/rep$rep/${1}_rep${rep}_5indv.msmc
  done
}

format_msmc sim_rec1
format_msmc sim_rec2
format_msmc sim_rec3
format_msmc sim_rec4
format_msmc sim_rec3_gc10_300bp
format_msmc sim_rec3_gc50_300bp
format_msmc sim_rec3_gc90_300bp
format_msmc sim_rec3_gc100_300bp
```

And finally run MSMC:

```bash
run_msmc() {
  for rep in {0..9}; do
    cd MSMC/$1/rep$rep/
    if [ ! -f ${1}_rep${rep}_${2}indv_msmc.loop.txt ]; then
      msmc2 -o ${1}_rep${rep}_${2}indv_msmc ${1}_rep${rep}_${2}indv.msmc
    fi
    cd ../../..
  done
}

for indv in 1 5; do
  run_msmc sim_rec1 $indv
  run_msmc sim_rec2 $indv
  run_msmc sim_rec3 $indv
  run_msmc sim_rec4 $indv
  run_msmc sim_rec3_gc10_300bp $indv
  run_msmc sim_rec3_gc50_300bp $indv
  run_msmc sim_rec3_gc90_300bp $indv
  run_msmc sim_rec3_gc100_300bp $indv
done
```

Try with less time intervals (default was 1*2+25*1+1*2+1*3):

```bash
run_msmc_16t() {
  for rep in {0..9}; do
    cd MSMC/$1/rep$rep/
    if [ ! -f ${1}_rep${rep}_${2}indv_msmc_16t.loop.txt ]; then
      msmc2 -o ${1}_rep${rep}_${2}indv_msmc_16t \
            -p 1*2+11*1+1*3 \
            ${1}_rep${rep}_${2}indv.msmc
    fi
    cd ../../..
  done
}

for indv in 1 5; do
  run_msmc_16t sim_rec1 $indv
  run_msmc_16t sim_rec2 $indv
  run_msmc_16t sim_rec3 $indv
  run_msmc_16t sim_rec4 $indv
  run_msmc_16t sim_rec3_gc10_300bp $indv
  run_msmc_16t sim_rec3_gc50_300bp $indv
  run_msmc_16t sim_rec3_gc90_300bp $indv
  run_msmc_16t sim_rec3_gc100_300bp $indv
done
```

Estimate rho with iSMC
======================

We compare with iSMC (1 and 5 individuals only)

For iSMC, extract selected individuals:

```bash
extract_ismc() {
  for rep in {0..9}; do
    echo "$1 $rep"
    cd Output/$1/rep$rep/
    vcftools --gzvcf ${1}_rep${rep}.vcf.gz --indv tsk_0 --maf 0.00001 --out ${1}_rep${rep}_1indv --recode
    bgzip -f ${1}_rep${rep}_1indv.recode.vcf
    vcftools --gzvcf ${1}_rep${rep}.vcf.gz --indv tsk_0 --indv tsk_1 --indv tsk_2 --indv tsk_3 --indv tsk_4 --maf 0.00001 --out ${1}_rep${rep}_5indv --recode
    bgzip -f ${1}_rep${rep}_5indv.recode.vcf
    cd ../../..
  done
}

extract_ismc sim_rec1
extract_ismc sim_rec2
extract_ismc sim_rec3
extract_ismc sim_rec4
extract_ismc sim_rec3_gc10_300bp
extract_ismc sim_rec3_gc50_300bp
extract_ismc sim_rec3_gc90_300bp
extract_ismc sim_rec3_gc100_300bp
```

Then generate the tab files:

```bash
format_ismc() {
  for rep in {0..9}; do
    echo "$1 $rep"
    mkdir -p iSMC/$1/rep${rep}/
    python3.9 ../../generate_tab.py Output/$1/rep${rep}/${1}_rep${rep}_1indv.recode.vcf.gz > iSMC/$1/rep${rep}/${1}_rep${rep}_1indv.tab
    python3.9 ../../generate_tab.py Output/$1/rep${rep}/${1}_rep${rep}_5indv.recode.vcf.gz > iSMC/$1/rep${rep}/${1}_rep${rep}_5indv.tab
  done
}

format_ismc sim_rec1
format_ismc sim_rec2
format_ismc sim_rec3
format_ismc sim_rec4
format_ismc sim_rec3_gc10_300bp
format_ismc sim_rec3_gc50_300bp
format_ismc sim_rec3_gc90_300bp
format_ismc sim_rec3_gc100_300bp
```

Then run iSMC:

```bash
run_ismc() {
  indv=$2
  for rep in {0..9}; do
    cd iSMC/$1/rep${rep}/
    if [ -f ${1}_rep${rep}_${indv}indv_estimates.txt ]; then
      echo "iSMC results found for ${1}_rep${rep}_${indv}indv. Skipping."
    else
      ismc param=../../../../../ismc_opt_homogeneous.bpp SIM=$1 REP=$rep INDV=_${indv}indv
      rm -rf ziphmm_*
    fi 
    cd ../../../
  done
}


run_ismc sim_rec1 1
run_ismc sim_rec2 1
run_ismc sim_rec3 1
run_ismc sim_rec4 1
run_ismc sim_rec3_gc10_300bp 1
run_ismc sim_rec3_gc50_300bp 1
run_ismc sim_rec3_gc90_300bp 1
run_ismc sim_rec3_gc100_300bp 1

run_ismc sim_rec1 5
run_ismc sim_rec2 5
run_ismc sim_rec3 5
run_ismc sim_rec4 5
run_ismc sim_rec3_gc10_300bp 5
run_ismc sim_rec3_gc50_300bp 5
run_ismc sim_rec3_gc90_300bp 5
run_ismc sim_rec3_gc100_300bp 5
```

Run iSMC with variable rho:

```bash
run_ismc_gamma5() {
  indv=$2
  for rep in {0..9}; do
    cd iSMC/$1/rep${rep}/
    if [ -f ${1}_rep${rep}_${indv}indv_gamma5_estimates.txt ]; then
      echo "iSMC results found for ${1}_rep${rep}_${indv}indv_gamma5. Skipping."
    else
      ismc param=../../../../../ismc_opt_gamma.bpp SIM=$1 REP=${rep} INDV=_${indv}indv
      rm -rf ziphmm_* 
    fi
    cd ../../../
  done
}

run_ismc_gamma5 sim_rec1 1
run_ismc_gamma5 sim_rec2 1
run_ismc_gamma5 sim_rec3 1
run_ismc_gamma5 sim_rec4 1
run_ismc_gamma5 sim_rec3_gc10_300bp 1
run_ismc_gamma5 sim_rec3_gc50_300bp 1
run_ismc_gamma5 sim_rec3_gc90_300bp 1
run_ismc_gamma5 sim_rec3_gc100_300bp 1

run_ismc_gamma5 sim_rec1 5
run_ismc_gamma5 sim_rec2 5
run_ismc_gamma5 sim_rec3 5
run_ismc_gamma5 sim_rec4 5
run_ismc_gamma5 sim_rec3_gc10_300bp 5
run_ismc_gamma5 sim_rec3_gc50_300bp 5
run_ismc_gamma5 sim_rec3_gc90_300bp 5
run_ismc_gamma5 sim_rec3_gc100_300bp 5
```

We also do the posterior decoding in order to get posterior estimates:

```bash
run_ismc_gamma5_decode() {
  indv=$2
  for rep in {0..9}; do
    cd iSMC/$1/rep${rep}/
    if [ -f ${1}_rep${rep}_${indv}indv_gamma5_diploid_decoding_labels.txt ]; then
      echo "iSMC results found for ${1}_rep${rep}_${indv}indv_gamma5. Skipping."
    else
      ismc param=../../../../../ismc_opt_gamma_decode.bpp SIM=$1 REP=${rep} INDV=_${indv}indv
      rm -rf ziphmm_* 
    fi
    cd ../../../
  done
}

run_ismc_gamma5_decode sim_rec1 1
run_ismc_gamma5_decode sim_rec2 1
run_ismc_gamma5_decode sim_rec3 1
run_ismc_gamma5_decode sim_rec4 1
run_ismc_gamma5_decode sim_rec3_gc10_300bp 1
run_ismc_gamma5_decode sim_rec3_gc50_300bp 1
run_ismc_gamma5_decode sim_rec3_gc90_300bp 1
run_ismc_gamma5_decode sim_rec3_gc100_300bp 1

run_ismc_gamma5_decode sim_rec1 5
run_ismc_gamma5_decode sim_rec2 5
run_ismc_gamma5_decode sim_rec3 5
run_ismc_gamma5_decode sim_rec4 5
run_ismc_gamma5_decode sim_rec3_gc10_300bp 5
run_ismc_gamma5_decode sim_rec3_gc50_300bp 5
run_ismc_gamma5_decode sim_rec3_gc90_300bp 5
run_ismc_gamma5_decode sim_rec3_gc100_300bp 5
```

A SLURM version:

```bash
slurm_ismc_gamma5_decode() {
  indv=$2
  counter=$3
  for rep in {0..9}; do
    echo "if [ \$SLURM_ARRAY_TASK_ID == ${counter} ]; then" >> $FILE
    echo "  cd iSMC/$1/rep${rep}/" >> $FILE
    echo "  ismc param=../../../../../ismc_opt_gamma_decode.bpp SIM=$1 REP=${rep} INDV=_${indv}indv" >> $FILE
    echo "  rm -rf ziphmm_*" >> $FILE
    echo "  cd ../../../" >> $FILE
    echo "fi" >> $FILE
    counter=$((counter+1))
  done
}

mkdir -p log/err
mkdir -p log/out

FILE=slurm_ismc_gamma5_decode_5indv.sh
rm $FILE

echo "#! /bin/bash" > $FILE
echo " " >> $FILE
echo "#SBATCH --job-name=rhosmcDecode5indv" >> $FILE
echo "#SBATCH --ntasks=10" >> $FILE
echo "#SBATCH --nodes=1" >> $FILE
echo "#SBATCH --array=1-80" >> $FILE
echo "#SBATCH --time=2-00:00:00" >> $FILE
echo "#SBATCH --mem=128G" >> $FILE
echo "#SBATCH --error=log/err/ismc_gamma5_decode_5indv.%J.err" >> $FILE
echo "#SBATCH --output=log/out/ismc_gamma5_decode_5indv.%J.out" >> $FILE
echo "#SBATCH --mail-type=ALL" >> $FILE
echo "#SBATCH --mail-user=dutheil@evolbio.mpg.de" >> $FILE
echo "#SBATCH --partition=standard" >> $FILE
echo "export PATH=$PATH:$HOME/.local/bin" >> $FILE

slurm_ismc_gamma5_decode sim_rec1 5 1
slurm_ismc_gamma5_decode sim_rec2 5 11
slurm_ismc_gamma5_decode sim_rec3 5 21
slurm_ismc_gamma5_decode sim_rec4 5 31
slurm_ismc_gamma5_decode sim_rec3_gc10_300bp 5 41
slurm_ismc_gamma5_decode sim_rec3_gc50_300bp 5 51
slurm_ismc_gamma5_decode sim_rec3_gc90_300bp 5 61
slurm_ismc_gamma5_decode sim_rec3_gc100_300bp 5 71
```

Get estimates per windows:

```bash
run_ismc_gamma5_mapper() {
  indv=$2
  for rep in {0..9}; do
    cd iSMC/$1/rep${rep}/
    ismc_mapper param=../../../../../ismc_opt_gamma_mapper.bpp SIM=$1 REP=${rep} INDV=_${indv}indv
    cd ../../../
  done
}

run_ismc_gamma5_mapper sim_rec1 1
run_ismc_gamma5_mapper sim_rec2 1
run_ismc_gamma5_mapper sim_rec3 1
run_ismc_gamma5_mapper sim_rec4 1
run_ismc_gamma5_mapper sim_rec3_gc10_300bp 1
run_ismc_gamma5_mapper sim_rec3_gc50_300bp 1
run_ismc_gamma5_mapper sim_rec3_gc90_300bp 1
run_ismc_gamma5_mapper sim_rec3_gc100_300bp 1

run_ismc_gamma5_mapper sim_rec1 5
run_ismc_gamma5_mapper sim_rec2 5
run_ismc_gamma5_mapper sim_rec3 5
run_ismc_gamma5_mapper sim_rec4 5
run_ismc_gamma5_mapper sim_rec3_gc10_300bp 5
run_ismc_gamma5_mapper sim_rec3_gc50_300bp 5
run_ismc_gamma5_mapper sim_rec3_gc90_300bp 5
run_ismc_gamma5_mapper sim_rec3_gc100_300bp 5
```



Estimate rho using LDhat
========================

Extract 50 individuals:

```bash
extract50() {
  for rep in {0..9}; do
    echo "$1 $rep"
    cd Output/$1/rep$rep/
    vcftools --gzvcf ${1}_rep${rep}.vcf.gz --keep ../../../../../50individuals.txt --maf 0.00001 --out ${1}_rep${rep}_50indv --recode
    bgzip -f ${1}_rep${rep}_50indv.recode.vcf
    cd ../../..
  done
}

extract50 sim_rec1
extract50 sim_rec2
extract50 sim_rec3
extract50 sim_rec4
extract50 sim_rec3_gc10_300bp
extract50 sim_rec3_gc50_300bp
extract50 sim_rec3_gc90_300bp
extract50 sim_rec3_gc100_300bp
```

We first need to convert the VCF to LDhat input format:

```bash
format_ldhat() {
  indv=$2
  for rep in {0..9}; do
    echo "$rec $rep"
    mkdir -p LDhat/$1/rep$rep
    vcftools --gzvcf Output/$1/rep$rep/${1}_rep${rep}_${indv}indv.recode.vcf.gz \
             --chr 1 --phased --ldhat \
             --out LDhat/$1/rep$rep/${1}_rep${rep}_${indv}indv
  done
}

format_ldhat sim_rec1 5
format_ldhat sim_rec2 5
format_ldhat sim_rec3 5
format_ldhat sim_rec4 5
format_ldhat sim_rec3_gc10_300bp 5
format_ldhat sim_rec3_gc50_300bp 5
format_ldhat sim_rec3_gc90_300bp 5
format_ldhat sim_rec3_gc100_300bp 5
```

Now with 50 individuals:

```bash
format_ldhat sim_rec1 50
format_ldhat sim_rec2 50
format_ldhat sim_rec3 50
format_ldhat sim_rec4 50
format_ldhat sim_rec3_gc10_300bp 50
format_ldhat sim_rec3_gc50_300bp 50
format_ldhat sim_rec3_gc90_300bp 50
format_ldhat sim_rec3_gc100_300bp 50
```

Run interval:

```bash
run_ldhat() {
  indv=$2
  for rep in {0..9}; do
    cd LDhat/$1/rep$rep/
    if [ -f res_${indv}indv.txt ]; then
      echo "LDhat results found for ${1}_rep${rep}_${indv}indv. Skipping."
    else
      echo "Sim: $1 Rep: $rep Indv: $indv"
      ~/.local/bin/interval -seq ${1}_rep${rep}_${indv}indv.ldhat.sites \
                            -loc ${1}_rep${rep}_${indv}indv.ldhat.locs \
                            -lk ../../../../../lk_n$((2*indv))_t0.001 \
                            -its 10000000 -bpen 5 -samp 5000 \
                            -concise
      ~/.local/bin/stat -input rates.txt \
                        -burn 20 \
                        -loc ${1}_rep${rep}_${indv}indv.ldhat.locs
      mv res.txt res_${indv}indv.txt
      rm rates.txt
    fi
    cd ../../..
  done
}

run_ldhat sim_rec1 5 
run_ldhat sim_rec2 5 
run_ldhat sim_rec3 5
run_ldhat sim_rec4 5
run_ldhat sim_rec3_gc10_300bp 5
run_ldhat sim_rec3_gc50_300bp 5
run_ldhat sim_rec3_gc90_300bp 5
run_ldhat sim_rec3_gc100_300bp `
```

50 individuals:

```bash
sbatch runLDhat.sh
```

Estimate rho using Pyrho
========================

Run pyrho with default parameters:

```bash
mkdir Pyrho
run_pyrho() {
  indv=$2
  cd Pyrho
  for rep in {0..9}; do
    mkdir -p ${1}/rep${rep}
    input="../Output/$1/rep$rep/${1}_rep${rep}_${indv}indv.recode.vcf.gz"
    if [ $indv -eq 100 ]; then
      input="../Output/$1/rep$rep/${1}_rep${rep}.vcf.gz"
    fi
    pyrho optimize --vcffile $input \
                   --tablefile ../../../DecreasingPopSize_$((indv * 2)).hdf \
                   --ploidy 1 \
                   --numthreads 20 \
                   --logfile . \
                   --outfile ${1}/rep${rep}/${1}_rep${rep}_${indv}indv.rmap
  done
  cd ..
}

run_pyrho sim_rec1 5
run_pyrho sim_rec2 5
run_pyrho sim_rec3 5
run_pyrho sim_rec4 5
run_pyrho sim_rec3_gc10_300bp 5
run_pyrho sim_rec3_gc50_300bp 5
run_pyrho sim_rec3_gc90_300bp 5
run_pyrho sim_rec3_gc100_300bp 5
```

Now with 50 individuals (100 haploids):

```bash
run_pyrho sim_rec1 50
run_pyrho sim_rec2 50
run_pyrho sim_rec3 50
run_pyrho sim_rec4 50
run_pyrho sim_rec3_gc10_300bp 50
run_pyrho sim_rec3_gc50_300bp 50
run_pyrho sim_rec3_gc90_300bp 50
run_pyrho sim_rec3_gc100_300bp 50
```

Estimate rho using HeRho
========================

```bash
conda activate herho
mkdir Herho

run_herho() {
  indv=$2
  cd Herho
  for rep in {0..9}; do
    mkdir -p ${1}/rep${rep}
    input="../Output/$1/rep$rep/${1}_rep${rep}_${indv}indv.recode.vcf.gz"
    if [ $indv -eq 100 ]; then
      input="../Output/$1/rep$rep/${1}_rep${rep}.vcf.gz"
    fi
    python3.8 ../../../heRho_tally_pairwise_counts_vcf.py --vcf $input --file_prefix ${1}/rep${rep}/${1}_rep${rep}_${indv}indv
    python3.8 ../../../heRho_stand_alone.py --input ${1}/rep${rep}/${1}_rep${rep}_${indv}indv.heRho_tally_per_chromosome.tsv >& ${1}/rep${rep}/${1}_rep${rep}_${indv}indv.rec.txt
  done
  cd ..
}
```

With 1 individual:

```bash
run_herho sim_rec1 1
run_herho sim_rec2 1
run_herho sim_rec3 1
run_herho sim_rec4 1
run_herho sim_rec3_gc10_300bp 1
run_herho sim_rec3_gc50_300bp 1
run_herho sim_rec3_gc90_300bp 1
run_herho sim_rec3_gc100_300bp 1
```

With 5 individuals:

```bash
run_herho sim_rec1 5
run_herho sim_rec2 5
run_herho sim_rec3 5
run_herho sim_rec4 5
run_herho sim_rec3_gc10_300bp 5
run_herho sim_rec3_gc50_300bp 5
run_herho sim_rec3_gc90_300bp 5
run_herho sim_rec3_gc100_300bp 5
```

With 50 individuals:

```bash
run_herho sim_rec1 50 #rep6 leads to an error, unclear why :(
run_herho sim_rec2 50
run_herho sim_rec3 50
run_herho sim_rec4 50 #error in rep7
run_herho sim_rec3_gc10_300bp 50
run_herho sim_rec3_gc50_300bp 50
run_herho sim_rec3_gc90_300bp 50
run_herho sim_rec3_gc100_300bp 50
```

