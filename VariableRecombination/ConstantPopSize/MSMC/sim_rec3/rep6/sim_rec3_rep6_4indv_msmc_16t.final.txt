time_index	left_time_boundary	right_time_boundary	lambda
0	0	6.13595e-06	34.4251
1	6.13595e-06	1.64717e-05	34.4251
2	1.64717e-05	3.38817e-05	445.55
3	3.38817e-05	6.32082e-05	421.729
4	6.32082e-05	0.000112607	414.802
5	0.000112607	0.000195818	344.783
6	0.000195818	0.000335983	346.224
7	0.000335983	0.000572084	366.269
8	0.000572084	0.000969786	385.059
9	0.000969786	0.0016397	411.572
10	0.0016397	0.00276813	436.677
11	0.00276813	0.00466893	421.629
12	0.00466893	0.00787075	437.892
13	0.00787075	0.0132641	311.782
14	0.0132641	0.0223489	311.782
15	0.0223489	inf	311.782
