#! /usr/bin/python3
import numpy
import msprime
import getopt
import sys
import os
import gzip

def main(argv):

  unix_opt = "r:c:t:o:"
  full_opt = ["rec=", "gc-prop=", "gc-track=", "output="]
  try:
    arguments, values = getopt.getopt(argv, unix_opt, full_opt)
  except getopt.error as err:
    print(str(err))
    sys.exit(2)



  # Default values:
  u = 1.25e-8
  r = [ x * 1.5e-8 for x in [ 0.1, 0.5, 1, 5 ] ]
  gc_prop = 0
  gc_track = 300
  n = 100 
  L = int(1e+7)
  N1 = 100000
  N2 = 1000
  T1 = 0 # End of size change (present)
  T2 = 1000 # Begining of size change (past)
  G = -numpy.log(N2 / N1) / (T2 - T1)
  seed = 42
  n_rep = 10
  output = 'sim'

  for arg, val in arguments:
    if arg in ("-r", "--rec"):
      r = float(val)
    elif arg in ("-c", "--gc-prop"):
      gc_prop = float(val)
    elif arg in ("-t", "--gc-track"):
      gc_track = float(val)
    elif arg in ("-o", "--output"):
      output = val

  demography = msprime.Demography()
  demography.add_population(name = "Pop", initial_size = N1, growth_rate = G)
  demography.add_population_parameters_change(time=T2, population="Pop", growth_rate = 0)

  dd = msprime.DemographyDebugger(demography = demography)
  dd.print_history()
  
  mutation_model = msprime.InfiniteSites(msprime.NUCLEOTIDES)

  # Generate a random recombination map:
  numpy.random.seed(seed)
  pos = 0
  breakpoints = []
  rates = []
  while pos < L:
    breakpoints.append(pos)
    rec = numpy.random.exponential(1)
    rates.append(rec)
    pos = pos + numpy.random.geometric(0.0001)
  breakpoints.append(L)
  rates.append(0)

  # We write it to a file:
  os.makedirs("Output/%s" % output, exist_ok = True)
  with open("Output/%s/RecombinationMap.csv" % output, 'w') as handle:
    for i, pos in enumerate(breakpoints):
      handle.write("%f,%f\n" % (pos, rates[i]))
  rates.pop()

  # Now simulate sequences:
  with open("Output/%s/AverageRates.csv" % output, 'w') as handle:
    handle.write("Rho,MeanRho,GC,GCTrack\n")
    print("- Simulating ARG with recombination rate rho=%f, among which %f percent is gene conversion." % (4*r*N1, gc_prop*100))
    recmap = msprime.RateMap(position = breakpoints, rate = [ x*r*(1-gc_prop) for x in rates ])
    print("- Mean recombination rate=%f" % (4*N1*recmap.mean_rate))
    handle.write("%f,%f,%f,%f\n" % (4*N1*r*(1-gc_prop), 4*N1*recmap.mean_rate*(1-gc_prop), 4*N1*r*gc_prop, gc_track))

  tss = msprime.sim_ancestry(
      samples = {"Pop": n},
      demography = demography,
      discrete_genome = True,
      recombination_rate = recmap,
      gene_conversion_rate = r * gc_prop,
      gene_conversion_tract_length = gc_track,
      model = "hudson",
      num_replicates = n_rep,
      random_seed = seed)
    
  for i, ts in enumerate(tss):
    print("  * Simulating replicate %i" % i)
    tsm = msprime.mutate(ts, rate = u, model = mutation_model, random_seed = seed)
    
    dir_path = "Output/%s/rep%i/" % (output, i)
    os.makedirs(dir_path, exist_ok = True)
    with gzip.open("%s/%s_rep%i.vcf.gz" % (dir_path, output, i), "wt") as vcf_file:
      tsm.write_vcf(vcf_file, position_transform = "legacy") # 'legacy' is used to avoid SNPs at the same position

if __name__ == "__main__":
  main(sys.argv[1:])
