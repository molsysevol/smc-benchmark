#!/bin/bash

#SBATCH --job-name=LDhat
#SBATCH --ntasks=1 # num of cores
#SBATCH --nodes=1
#SBATCH --time=48:00:00 # in hours
#SBATCH --mem=64G 
#SBATCH --error=slurm_ldhat5_log.%J.err
#SBATCH --output=slurm_ldhat5_log.%J.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=dutheil@evolbio.mpg.de
#SBATCH --partition=medium 
#SBATCH --array=1-90 

#SAMPLE_SIZE=50
SAMPLE_SIZE=5

run_ldhat() {
  indv=$2
  rep=$3
  cd LDhat/$1/rep$rep/
  if [ -f res_${indv}indv.txt ]; then
    echo "LDhat results found for ${1}_rep${rep}_${indv}indv. Skipping."
  else
    echo "Sim: $1 Rep: $rep Indv: $indv"
    ~/.local/bin/interval -seq ${1}_rep${rep}_${indv}indv.ldhat.sites \
                          -loc ${1}_rep${rep}_${indv}indv.ldhat.locs \
                          -lk ../../../../../lk_n$((2*indv))_t0.001 \
                          -its 10000000 -bpen 5 -samp 5000 \
                          -concise
    ~/.local/bin/stat -input rates.txt \
                      -burn 20 \
                      -loc ${1}_rep${rep}_${indv}indv.ldhat.locs
    mv res.txt res_${indv}indv.txt
    rm rates.txt
  fi
  cd ../../..
}
if [[ ${SLURM_ARRAY_TASK_ID} -le 10 ]]; then
  run_ldhat sim_norec $SAMPLE_SIZE $(( SLURM_ARRAY_TASK_ID - 1 ))
elif [[ ${SLURM_ARRAY_TASK_ID} -le 20 ]]; then
  run_ldhat sim_rec1 $SAMPLE_SIZE $(( SLURM_ARRAY_TASK_ID - 11 ))
elif [[ ${SLURM_ARRAY_TASK_ID} -le 30 ]]; then
  run_ldhat sim_rec2 $SAMPLE_SIZE $(( SLURM_ARRAY_TASK_ID - 21 ))
elif [[ ${SLURM_ARRAY_TASK_ID} -le 40 ]]; then
  run_ldhat sim_rec3 $SAMPLE_SIZE $(( SLURM_ARRAY_TASK_ID - 31 ))
elif [[ ${SLURM_ARRAY_TASK_ID} -le 50 ]]; then
  run_ldhat sim_rec4 $SAMPLE_SIZE $(( SLURM_ARRAY_TASK_ID - 41 ))
elif [[ ${SLURM_ARRAY_TASK_ID} -le 60 ]]; then
  run_ldhat sim_rec3_gc10_300bp $SAMPLE_SIZE $(( SLURM_ARRAY_TASK_ID - 51 ))
elif [[ ${SLURM_ARRAY_TASK_ID} -le 70 ]]; then
  run_ldhat sim_rec3_gc50_300bp $SAMPLE_SIZE $(( SLURM_ARRAY_TASK_ID - 61 ))
elif [[ ${SLURM_ARRAY_TASK_ID} -le 80 ]]; then
  run_ldhat sim_rec3_gc90_300bp $SAMPLE_SIZE $(( SLURM_ARRAY_TASK_ID - 71 ))
elif [[ ${SLURM_ARRAY_TASK_ID} -le 90 ]]; then
  run_ldhat sim_rec3_gc100_300bp $SAMPLE_SIZE $(( SLURM_ARRAY_TASK_ID - 81 ))
else
  echo "Nothing to do here."
fi

