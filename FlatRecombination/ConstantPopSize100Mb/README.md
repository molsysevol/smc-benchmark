<!-- Created on 19/03/21 by jdutheil -->

Data simulation
===============

```bash
python3.10 simulate.py --rec=0.0 --output=sim_norec
python3.10 simulate.py --rec=0.15e-8 --output=sim_rec1
python3.10 simulate.py --rec=0.75e-8 --output=sim_rec2
python3.10 simulate.py --rec=1.5e-8 --output=sim_rec3
python3.10 simulate.py --rec=7.5e-8 --output=sim_rec4
python3.10 simulate.py --rec=1.5e-8 --gc-prop=0.1 --gc-track=300  --output=sim_rec3_gc10_300bp
python3.10 simulate.py --rec=1.5e-8 --gc-prop=0.5 --gc-track=300  --output=sim_rec3_gc50_300bp
python3.10 simulate.py --rec=1.5e-8 --gc-prop=0.9 --gc-track=300  --output=sim_rec3_gc90_300bp
python3.10 simulate.py --rec=1.5e-8 --gc-prop=1.0 --gc-track=300  --output=sim_rec3_gc100_300bp
```

Compute genetic diversity (using all individuals):

```bash
compute_diversity() {
  echo "Computing diversity for ${1}..."
  for rep in {0..9}; do
    mkdir -p Diversity/$1/rep$rep/
    cd Diversity/$1/rep$rep/

    echo "$1, replicate $rep"

    vcftools --gzvcf ../../../Output/$1/rep$rep/${1}_rep${rep}.vcf.gz \
             --window-pi 100000000 \
             --out ${1}_rep${rep}_50indv

    cd ../../..
  done
}

compute_diversity sim_norec
compute_diversity sim_rec1
compute_diversity sim_rec2
compute_diversity sim_rec3
compute_diversity sim_rec4
compute_diversity sim_rec3_gc10_300bp
compute_diversity sim_rec3_gc50_300bp
compute_diversity sim_rec3_gc90_300bp
compute_diversity sim_rec3_gc100_300bp
```

Estimate rho using MSMC
=======================

For MSMC, extract 5 individuals:

```bash
extract_individuals() {
  echo "Extracting individuals for ${1}..."
  for rep in {0..9}; do
    for i in tsk_0 tsk_1 tsk_2 tsk_3 tsk_4; do
      echo "$rec $rep $i"
      cd Output/$1/rep$rep/
      vcftools --gzvcf ${1}_rep${rep}.vcf.gz --indv $i --out ${1}_rep${rep}_${i} --recode
      bgzip -f ${1}_rep${rep}_${i}.recode.vcf
      cd ../../..
    done
  done
}

extract_individuals sim_norec
extract_individuals sim_rec1
extract_individuals sim_rec2
extract_individuals sim_rec3
extract_individuals sim_rec4
extract_individuals sim_rec3_gc10_300bp
extract_individuals sim_rec3_gc50_300bp
extract_individuals sim_rec3_gc90_300bp
extract_individuals sim_rec3_gc100_300bp
```

We then generate the MSMC input files, combining all individuals:

```bash
format_msmc() {
  echo "Formating MSMC files for ${1}..."
  for rep in {0..9}; do
    mkdir -p MSMC/$1/rep$rep/
    python3 ../../generate_multihetsep.py \
        Output/$1/rep$rep/${1}_rep${rep}_tsk_0.recode.vcf.gz \
        > MSMC/$1/rep$rep/${1}_rep${rep}_1indv.msmc
    python3 ../../generate_multihetsep.py \
        Output/$1/rep$rep/${1}_rep${rep}_tsk_0.recode.vcf.gz \
        Output/$1/rep$rep/${1}_rep${rep}_tsk_1.recode.vcf.gz \
        > MSMC/$1/rep$rep/${1}_rep${rep}_2indv.msmc
    python3 ../../generate_multihetsep.py \
        Output/$1/rep$rep/${1}_rep${rep}_tsk_0.recode.vcf.gz \
        Output/$1/rep$rep/${1}_rep${rep}_tsk_1.recode.vcf.gz \
        Output/$1/rep$rep/${1}_rep${rep}_tsk_2.recode.vcf.gz \
        > MSMC/$1/rep$rep/${1}_rep${rep}_3indv.msmc
    python3 ../../generate_multihetsep.py \
        Output/$1/rep$rep/${1}_rep${rep}_tsk_0.recode.vcf.gz \
        Output/$1/rep$rep/${1}_rep${rep}_tsk_1.recode.vcf.gz \
        Output/$1/rep$rep/${1}_rep${rep}_tsk_2.recode.vcf.gz \
        Output/$1/rep$rep/${1}_rep${rep}_tsk_3.recode.vcf.gz \
        > MSMC/$1/rep$rep/${1}_rep${rep}_4indv.msmc
    python3 ../../generate_multihetsep.py \
        Output/$1/rep$rep/${1}_rep${rep}_tsk_0.recode.vcf.gz \
        Output/$1/rep$rep/${1}_rep${rep}_tsk_1.recode.vcf.gz \
        Output/$1/rep$rep/${1}_rep${rep}_tsk_2.recode.vcf.gz \
        Output/$1/rep$rep/${1}_rep${rep}_tsk_3.recode.vcf.gz \
        Output/$1/rep$rep/${1}_rep${rep}_tsk_4.recode.vcf.gz \
        > MSMC/$1/rep$rep/${1}_rep${rep}_5indv.msmc
  done
}

format_msmc sim_norec
format_msmc sim_rec1
format_msmc sim_rec2
format_msmc sim_rec3
format_msmc sim_rec4
format_msmc sim_rec3_gc10_300bp
format_msmc sim_rec3_gc50_300bp
format_msmc sim_rec3_gc90_300bp
format_msmc sim_rec3_gc100_300bp
```

And finally run MSMC:

```bash
run_msmc() {
  for rep in {0..9}; do
    cd MSMC/$1/rep$rep/
    if [ ! -f ${1}_rep${rep}_${2}indv_msmc.loop.txt ]; then
      msmc2 -o ${1}_rep${rep}_${2}indv_msmc ${1}_rep${rep}_${2}indv.msmc
    fi
    cd ../../..
  done
}

for indv in {1..5}; do
  run_msmc sim_norec $indv
  run_msmc sim_rec1 $indv
  run_msmc sim_rec2 $indv
  run_msmc sim_rec3 $indv
  run_msmc sim_rec4 $indv
  run_msmc sim_rec3_gc10_300bp $indv
  run_msmc sim_rec3_gc50_300bp $indv
  run_msmc sim_rec3_gc90_300bp $indv
  run_msmc sim_rec3_gc100_300bp $indv
done
```

Note: cannot run with 5 individual, not enough memory!

Try with less time intervals (default was 1*2+25*1+1*2+1*3):

```bash
run_msmc_16t() {
  for rep in {0..9}; do
    cd MSMC/$1/rep$rep/
    if [ ! -f ${1}_rep${rep}_${2}indv_msmc_16t.loop.txt ]; then
      msmc2 -o ${1}_rep${rep}_${2}indv_msmc_16t \
            -p 1*2+11*1+1*3 \
            ${1}_rep${rep}_${2}indv.msmc
    fi
    cd ../../..
  done
}

for indv in {1..5}; do
  run_msmc_16t sim_norec $indv
  run_msmc_16t sim_rec1 $indv
  run_msmc_16t sim_rec2 $indv
  run_msmc_16t sim_rec3 $indv
  run_msmc_16t sim_rec4 $indv
  run_msmc_16t sim_rec3_gc10_300bp $indv
  run_msmc_16t sim_rec3_gc50_300bp $indv
  run_msmc_16t sim_rec3_gc90_300bp $indv
  run_msmc_16t sim_rec3_gc100_300bp $indv
done
```

Note: 5 indv also crashed here.

