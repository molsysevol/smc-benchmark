time_index	left_time_boundary	right_time_boundary	lambda
0	0	9.31536e-06	0.443734
1	9.31536e-06	2.44065e-05	0.443734
2	2.44065e-05	4.88547e-05	0.443734
3	4.88547e-05	8.84615e-05	0.443734
4	8.84615e-05	0.000152626	1.49712
5	0.000152626	0.000256574	194.668
6	0.000256574	0.000424973	316.096
7	0.000424973	0.000697786	578.78
8	0.000697786	0.00113975	779.33
9	0.00113975	0.00185575	351.177
10	0.00185575	0.00301568	254.199
11	0.00301568	0.00489482	532.946
12	0.00489482	0.00793907	600.331
13	0.00793907	0.0128709	352.241
14	0.0128709	0.0208605	352.241
15	0.0208605	inf	352.241
