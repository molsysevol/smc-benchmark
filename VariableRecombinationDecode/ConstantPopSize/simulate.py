#! /usr/bin/python3
import numpy
import msprime
import getopt
import sys
import os
import gzip
import pandas

def main(argv):

  unix_opt = "r:c:t:o:"
  full_opt = ["rec=", "gc-prop=", "gc-track=", "output="]
  try:
    arguments, values = getopt.getopt(argv, unix_opt, full_opt)
  except getopt.error as err:
    print(str(err))
    sys.exit(2)



  # Default values:
  u = 1.25e-8
  r = [ x * 1.5e-8 for x in [ 0.1, 0.5, 1, 5 ] ]
  gc_prop = 0
  gc_track = 300
  n = 100 
  L = int(1e+7)
  N = 100000
  G = 0
  seed = 42
  n_rep = 10
  output = 'sim'

  for arg, val in arguments:
    if arg in ("-r", "--rec"):
      r = float(val)
    elif arg in ("-c", "--gc-prop"):
      gc_prop = float(val)
    elif arg in ("-t", "--gc-track"):
      gc_track = float(val)
    elif arg in ("-o", "--output"):
      output = val

  demography = msprime.Demography()
  demography.add_population(name = "Pop", initial_size = N, growth_rate = G)

  dd = msprime.DemographyDebugger(demography = demography)
  
  mutation_model = msprime.InfiniteSites(msprime.NUCLEOTIDES)

  # Generate a recombination map based on the DECODE map:
  rmap = pandas.read_csv("../RecombinationMapTemplate.csv")
  breakpoints = numpy.append(rmap.iloc[:,0].to_numpy(), L)
  m = rmap.iloc[:,1].mean()
  rates = [ x / m for x in rmap.iloc[:,1].to_numpy() ]

  # Now simulate sequences:
  os.makedirs("Output/%s/" % output, exist_ok = True)
  with open("Output/%s/AverageRates.csv" % output, 'w') as handle:
    handle.write("Rho,MeanRho,GC,GCTrack\n")
    print("- Simulating ARG with recombination rate rho=%f, among which %f percent is gene conversion." % (4*r*N, gc_prop*100))
    recmap = msprime.RateMap(position = breakpoints, rate = [ x*r*(1-gc_prop) for x in rates ])
    print("- Mean recombination rate=%f" % (4*N*recmap.mean_rate))
    handle.write("%f,%f,%f,%f\n" % (4*N*r*(1-gc_prop), 4*N*recmap.mean_rate*(1-gc_prop), 4*N*r*gc_prop, gc_track))

  tss = msprime.sim_ancestry(
      samples = {"Pop": n},
      demography = demography,
      discrete_genome = True,
      recombination_rate = recmap,
      gene_conversion_rate = r * gc_prop,
      gene_conversion_tract_length = gc_track,
      model = "hudson",
      num_replicates = n_rep,
      random_seed = seed)
    
  for i, ts in enumerate(tss):
    print("  * Simulating replicate %i" % i)
    tsm = msprime.mutate(ts, rate = u, model = mutation_model, random_seed = seed)
    
    dir_path = "Output/%s/rep%i/" % (output, i)
    os.makedirs(dir_path, exist_ok = True)
    with gzip.open("%s/%s_rep%i.vcf.gz" % (dir_path, output, i), "wt") as vcf_file:
      tsm.write_vcf(vcf_file, position_transform = "legacy") # 'legacy' is used to avoid SNPs at the same position

if __name__ == "__main__":
  main(sys.argv[1:])
