Uses a "real" recombination map for simulations.

Get the DECODE recombination map:

```bash
wget https://www.decode.com/additional/sex-averaged.rmap
```

The DECODE map is provided for 10kb windows. We get a sample of 1000 consecutive windows to get a 10 Mb region:

```r
rmap <- read.table("sex-averaged.rmap", header = TRUE)
rmap <- subset(rmap, chr == "chr1")
rmap <- rmap[1:1000,]
plot(stdrate~pos, rmap)
i <- seq(from = 0, by = 1e4, to = 1e7-1)
rmap <- data.frame(Start = i, Rate = rmap$stdrate)
write.table(rmap, 
    file = "RecombinationMapTemplate.csv", 
    row.names = FALSE, 
    sep = ",")
```
